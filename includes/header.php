<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="title" content="">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

	<!--[if IE]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<title>New Site</title>

	<link rel="shortcut icon" href="favicon.ico">
	<link rel="apple-touch-icon" href="apple-touch-icon-114x114.png">

	<link rel='stylesheet' href='css/layout.min.css' />
	

<!-- remove before production -->
<script src="http://localhost:35729/livereload.js?snipver=1"></script>

<!-- gulp/sass error styles -->
<style>
	body:before {
		background:#c0392b;
		display: block;
		color:#fff;
		padding:2%;
	}
</style>

</head>

<body id="debug">
	
	
<div id="page">
<div id="skiptocontent" class="visuallyhidden"><a href="#main">skip to content</a></div>

<header>
	<img src="webimages/aaf-header-v2.jpg" />
</header>