// load Gulp dependencies
var gulp = require('gulp'); 
var sass = require('gulp-ruby-sass');
var rename = require('gulp-rename');
var minifyCss = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');
var cmq = require('gulp-combine-media-queries');
var uglify = require('gulp-uglify');
var livereload = require('gulp-livereload');
var gutil = require('gulp-util');
var ftp = require('vinyl-ftp');

	// ftp upload setting
	var host = 'ftp.dvlhost.com';
	var user = 'raddysnashville@dvlhost.com';
	var password = '2x94I_sQnZP3=1';
	var localFilesGlob = ['css/layout.min.css','js/functions.min.js'];  
	var remoteFolder = '/wp/wp-content/themes/AAFNashville'



function onError(err) {
    console.log(err);
}


// watch SCSS
gulp.task('styles', function () {

  // compile scss
	return sass('sass/main.scss')
    .on('error', function (err) {
        console.error('Error!', err.message);
    })
    
    // automatically add needed browser prefixing
    .pipe(autoprefixer({
        browsers: ['> 5%', 'IE 9','last 2 versions'],
        cascade: false,
        remove: true
    }))
    
    // combine media queries/move to end of document
    .pipe(cmq({
      log: true
    }))
    
    // minify/compress css
    .pipe(minifyCss({
	    processImport: false,
	    debug: true,
	    keepBreaks: false
	  }))
	  
	  // rename in folder
		.pipe(rename('layout.min.css'))			
    .pipe(gulp.dest('css'))
    
    // re-inject styles into page
    .pipe(livereload());
});



// watch javascript
gulp.task('scripts', function() {
    return gulp.src('js/functions.js')
    	.pipe(uglify())
    	.pipe(rename('functions.min.js'))			
			.pipe(gulp.dest('js'))
      .pipe(livereload());
});




// watch files for changes
gulp.task('watch', function() {	
	livereload.listen();

  //watch and reload PHP and html
  gulp.watch('**/*.php').on('change', function(file) {
  	livereload.changed(file.path); 
  });
  
  gulp.watch('**/*.html').on('change', function(file) {
  	livereload.changed(file.path);
 	});

  gulp.watch('js/*.js', ['scripts']);
  gulp.watch('sass/*.scss', ['styles']);
});



// deploy via ftp 
gulp.task('deploy', function() {

    var connection = ftp.create( {
      host: host,
      user: user,
      password: password,
      parallel: 10,
      log: gutil.log
  });

    gulp.watch(localFilesGlob)
    .on('change', function(event) {
      console.log('Uploading file "' + event.path + '", ' + event.type);

    return gulp.src(localFilesGlob, { base: '.', buffer: false })
      .pipe(connection.newer(remoteFolder)) // only upload newer files 
      .pipe(connection.dest(remoteFolder));
  });
});


// Ready? Set... Go!
gulp.task('default', ['styles', 'scripts', 'watch', 'deploy']);	

// (for no FTP upload - local only - run "gulp local")
gulp.task('local', ['styles', 'scripts', 'watch']);	
