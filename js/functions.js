$(document).ready(function() {
	$('.popup-image').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		closeBtnInside: false,
		fixedContentPos: false
	});
	
	$('.popup-video').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,
		fixedContentPos: false
	});
	
	// smoothScroll
	$('a.scrollTo').on('click', function(event){			
	  event.preventDefault();
	  $('html,body').animate({scrollTop:$(this.hash).offset().top}, 1000);
	});
	
	
	// initalize audio player
	$('audio').audioPlayer();
	
});